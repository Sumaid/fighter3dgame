#include "main.h"
#include "line.h"
#include "circle.h"

#ifndef SEGDISPLAY_H
#define SEGDISPLAY_H

using namespace std;

class SegDisplay {
public:
    SegDisplay() {}
    SegDisplay(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    float thick;
    void draw(glm::mat4 VP, char input);
    void set_position(float x, float y);
    float speed;
    Line a,b,c,d,e,f,g;
    vector<Circle> circles;
    bounding_box_t bounding_box();
private:
};

#endif // SegDisplay_H
