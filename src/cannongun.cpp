#include "cannongun.h"
#include "main.h"

using namespace glm;

Cannongun::Cannongun(float x, float y, float z, float x_length, float y_length, float z_length, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->x_length = x_length;
    this->y_length = y_length;
    this->z_length = z_length;
    rotation_matrix = mat4(1.0f);
    acc = 0.01;
    speed = 0;
    GLfloat vertex_buffer_data[] = {
        -1.0f,-1.0f,-1.0f, 
        -1.0f,-1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f, 
        1.0f, 1.0f,-1.0f, 
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f, 
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        -1.0f,-1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f,-1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f,-1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f,-1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f,-1.0f, 1.0f
    };
    //for (int i=0; i<36*3; i++)
    //{
    //    vertex_buffer_data[i] += 1;
    //}
    this->object = create3DObject(GL_TRIANGLES, 12*3, vertex_buffer_data, color, GL_FILL);
}

void Cannongun::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 translate1 = glm::translate (glm::vec3(-1.0, -1.0, -1.0));    // glTranslatef
    glm::mat4 myScalingMatrix;
    float scalingfactor = 1.0f;
    myScalingMatrix[0] = vec4((float)x_length/2, 0.0, 0.0, 0.0);
    myScalingMatrix[1] = vec4(0.0, (float)y_length/2, 0.0, 0.0);
    myScalingMatrix[2] = vec4(0.0, 0.0, (float)z_length/2, 0.0);
    myScalingMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0f);    
    Matrices.model *= (translate * rotation_matrix * myScalingMatrix);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Cannongun::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

int Cannongun::tick() {
    speed += acc;
    position.y -= speed;
    if (position.y<=0)
        return 1;
    return 0;
}

bounding_box_t Cannongun::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = 0;
    box.x = x-x_length/2; box.y = y-y_length/2; box.z = z-z_length/2;
    box.x_length = x_length; 
    box.y_length = y_length;
    box.z_length = z_length;
    return box;
}

void Cannongun::align(glm::vec3 axis){
//	printf("axis.x: %f, axis.y: %f, axis.z:%f\n", axis.x, axis.y, axis.z);
//	z_ref1 = axis;
    //glm::vec3 axis = glm::vec3(axis.x*3, axis.y*3, axis.z*3);
	glm::vec3 temp = axis + glm::vec3(1,1,1);
	glm::vec3 temp_x = glm::normalize(temp);
	 temp_x = glm::normalize(glm::cross(axis, temp_x));
	glm::vec3 temp_z = glm::normalize(glm::cross(temp_x, axis));

	rotation_matrix[0] = glm::vec4(temp_x,0.0f);
	rotation_matrix[1] = glm::vec4(axis,0.0f);
	rotation_matrix[2] = glm::vec4(temp_z,0.0f);
	rotation_matrix[3] = glm::vec4(0.0f,0.0f,0.0f,1.0f);
}
