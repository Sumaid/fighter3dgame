#include "main.h"
#include "line.h"
#include "ring.h"
#include "cone.h"
#include "rectangle.h"

#ifndef VOLCANO_H
#define VOLCANO_H

using namespace std;

class Volcano {
public:
    Volcano() {}
    Volcano(float x, float y, float z);
    glm::vec3 position;
    float rotation;
    float thick;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    float speed;
    float baseradius;
    float mountain_radius;
    float mountain_length;
    Rect a,b,c,d,e,f;
    Ring base;
    Cone mountain;
    bounding_box_t bounding_box();
private:
};

#endif // VOLCANO_H
