#define GLM_ENABLE_EXPERIMENTAL
#include "main.h"

#ifndef RECT_H
#define RECT_H


class Rect {
public:
    Rect() {}
    Rect(float x, float y, float z, float length, float breadth, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    int tick(float flag);
    int lefttick(float flag);
    int move(float flag);
    double speed;
    float length, breadth;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // RECT_H
