#include "main.h"

#ifndef CYLINDER_H
#define CYLINDER_H


class Cylinder {
public:
    Cylinder() {}
    Cylinder(float x, float y, float z, float length, float radius, color_t color);
    glm::vec3 position, z_ref1, x_ref1, y_ref1;
    float rotation, speed;
    float length, radius;
    void draw(glm::mat4 VP);
    glm::mat4 translate, rotation_matrix;
    void set_position(float x, float y, float z);
    void align(glm::vec3 axis);
    int tick();
    void move(float amount);
    bounding_box_t bounding_box();

private:
    VAO *object;
};

#endif // CYLINDER_H
