#include <vector>
#include "ring.h"
#include "cone.h"
#include "volcano.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

Volcano::Volcano(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
    this->speed = 0.01;
    thick = 0.03f;
    baseradius = 5.0;
    mountain_radius = 2.75;
    mountain_length = 6.75;
    base = Ring(x, y, z, baseradius, COLOR_TREEGREEN);
    base.rotate_matrix(90.0f, glm::vec3(1,0,0));
    mountain = Cone(x, y, z, mountain_length, mountain_radius, COLOR_BROWN);
}

void Volcano::draw(glm::mat4 VP) {
    base.draw(VP);
    mountain.draw(VP);
}

void Volcano::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

bounding_box_t Volcano::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = 3;
    box.x = x; box.y = y; box.z = z;
    box.radius = baseradius;
    box.length = mountain_length;
    return box;
}
