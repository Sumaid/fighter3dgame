#include <vector>
#include "line.h"
#include "circle.h"
#include "segdisplay.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

SegDisplay::SegDisplay(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->speed = 0.01;
    thick = 0.03f;
    float length = 0.3;
    a = Line(x, y, x+length, y, thick, color);
    b = Line(x+length, y, x+length, y-length/2, thick, color);
    c = Line(x+length, y-length/2, x+length, y-length, thick, color);
    d = Line(x, y-length, x+length, y-length, thick, color);
    f = Line(x, y, x, y-length/2, thick, color);
    e = Line(x, y-length/2, x, y-length, thick, color);
    g = Line(x, y-length/2, x+length, y-length/2, thick, color);
}

void SegDisplay::draw(glm::mat4 VP, char input) {
    if (input=='0')
    {   
        a.draw(VP);
        b.draw(VP);
        c.draw(VP);
        d.draw(VP);
        e.draw(VP);
        f.draw(VP);
    }
    else if(input=='1')
    {
        b.draw(VP);
        c.draw(VP);
    }
    else if(input=='2')
    {
        a.draw(VP);
        b.draw(VP);
        g.draw(VP);
        d.draw(VP);
        e.draw(VP);
    }
    else if(input=='3')
    {
        a.draw(VP);
        b.draw(VP);
        g.draw(VP);
        d.draw(VP);
        c.draw(VP);
    }
    else if(input=='4')
    {
        f.draw(VP);
        g.draw(VP);
        b.draw(VP);
        c.draw(VP);
    }
    else if(input=='5')
    {
        f.draw(VP);
        g.draw(VP);
        a.draw(VP);
        c.draw(VP);
        d.draw(VP);
    }
    else if(input=='6')
    {
        f.draw(VP);
        g.draw(VP);
        a.draw(VP);
        c.draw(VP);
        d.draw(VP);
        e.draw(VP);
    }
    else if(input=='7')
    {
        a.draw(VP);
        b.draw(VP);
        c.draw(VP);
    }
    else if(input=='8')
    {
        a.draw(VP);
        b.draw(VP);
        c.draw(VP);
        d.draw(VP);
        e.draw(VP);
        f.draw(VP);
        g.draw(VP);
    }
    else if(input=='9')
    {
        a.draw(VP);
        b.draw(VP);
        c.draw(VP);
        d.draw(VP);
        f.draw(VP);
        g.draw(VP);
    }    
}

void SegDisplay::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}
