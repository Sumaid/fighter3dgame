#include "main.h"
#include "cylinder.h"
#include "cone.h"
#include <vector>

#ifndef PLANE_H
#define PLANE_H


class Plane {
public:
    Plane() {}
    Plane(float x, float y, float z, color_t color);
    glm::vec3 x_ref, y_ref, z_ref, z_ref1, y_ref1;
    glm::vec4 position;
    glm::mat4 rotation;
    float pitch, roll, yaw;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    void tick();
    void boost(int flag);
    void rotateall(float yaw, float pitch, float roll);
    double speed;
    float acceleration;
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // Jet_H
