#define GLM_ENABLE_EXPERIMENTAL
#include <iostream>
#include <cmath>
#include <vector>
#include <utility>
#include <stdlib.h> 
#include <stdio.h>
#include "main.h"
#include "timer.h"
#include "jet.h"
#include "plane.h"
#include "line.h"
#include "seg7.h"
#include "circle.h"
#include "islands.h"
#include "sea.h"
#include "rectangle.h"
#include "segdisplay.h"
#include "fuelbar.h"
#include "cuboid.h"
#include "cone.h"
#include "cylinder.h"
#include "ring.h"
#include "volcano.h"
#include "enemy.h"
#include "cannon.h"
#include "cannongun.h"
#include "arrow.h"
#include "arrow2d.h"

using namespace std;
using namespace glm;

GLMatrices Matrices;
GLuint     programID, programID_blender;
GLFWwindow *window;

Cuboid seacuboid;
Cone cone;
Arrow arrow;
Cylinder cylinder;
Plane Jet1;
Jet jet;
Island BigIsland, BigIsland1;
Line line;
Circle c1;
Fuelbar bar;
Sea seaobject;
Ring compassring1, compassring2;
Arrow2D compass;
vector<Cuboid> cans;
vector<Cuboid> bullets;
vector<Cuboid> bombs;
vector<Cylinder> missiles;
vector<Ring> rings;
vector<Volcano> volcanoes;
vector<Enemy> enemies;
vector<Cannon> cannons;
vector<Segment> segments;
vector<Cannongun> cannon_guns;
vector<Arrow> cannon_arrows;
vector<SegDisplay> segments_score, segments_speed, segments_height;
vector<Island> bigislandlist;

int score = 000;
float fuel = 100.0;
int speedtext = 21;
int altitude = 120;
int bullet_counter = 0;

float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
int directionflag=0;
float camera_rotation_angle = 200;
int cam_view=2;
float x_a, y_a, z_a;
float t_x, t_y, t_z;
float u_x, u_y, u_z;
//glm::vec3 position = glm::vec3( 0, 0, 5 ); 
float horizontalAngle = 3.14f;
float verticalAngle = 0.0f;

float speed = 3.0f;
float mouseSpeed = 0.01f;

Timer t60(1.0 / 60);

float max(float a, float b)
{
    if (a>b)
        return a;
    return b;
}

void init_score(float x, float y)
{
    segments.push_back(Segment(x, y, 'H'));
    segments.push_back(Segment(x+0.4, y, 'E'));
    segments.push_back(Segment(x+0.8, y, 'A'));
    segments.push_back(Segment(x+1.2, y, 'L'));
    segments.push_back(Segment(x+1.6, y, 'T'));
    segments.push_back(Segment(x+2.0, y, 'H'));
    segments.push_back(Segment(x+2.4, y, 'q'));
    segments_score.push_back(SegDisplay(x+2.8, y, COLOR_BLACK));
    segments_score.push_back(SegDisplay(x+3.2, y, COLOR_BLACK));
    segments_score.push_back(SegDisplay(x+3.6, y, COLOR_BLACK));
}

void init_speed(float x, float y)
{
    segments.push_back(Segment(x, y, 'S'));
    segments.push_back(Segment(x+0.4, y, 'P'));
    segments.push_back(Segment(x+0.8, y, 'E'));
    segments.push_back(Segment(x+1.2, y, 'E'));
    segments.push_back(Segment(x+1.6, y, '0'));
    segments.push_back(Segment(x+2.0, y, 'l'));
    segments.push_back(Segment(x+2.4, y, 'q'));
    segments_speed.push_back(SegDisplay(x+2.8, y, COLOR_BLACK));
    segments_speed.push_back(SegDisplay(x+3.2, y, COLOR_BLACK));
    segments_speed.push_back(SegDisplay(x+3.6, y, COLOR_BLACK));
}

void init_altitude(float x, float y)
{
    segments.push_back(Segment(x, y, 'H'));
    segments.push_back(Segment(x+0.4, y, 'I'));
    segments.push_back(Segment(x+0.8, y, 'E'));
    segments.push_back(Segment(x+1.2, y, 'G'));
    segments.push_back(Segment(x+1.6, y, 'H'));
    segments.push_back(Segment(x+2.0, y, 'T'));
    segments.push_back(Segment(x+2.4, y, 'q'));
    segments_height.push_back(SegDisplay(x+2.8, y, COLOR_BLACK));
    segments_height.push_back(SegDisplay(x+3.2, y, COLOR_BLACK));
    segments_height.push_back(SegDisplay(x+3.6, y, COLOR_BLACK));
}

void init_fuel(float x, float y)
{
    segments.push_back(Segment(x, y, 'F'));
    segments.push_back(Segment(x+0.4, y, 'U'));
    segments.push_back(Segment(x+0.8, y, 'E'));
    segments.push_back(Segment(x+1.2, y, 'L'));
    segments.push_back(Segment(x+1.6, y, 'q'));
    bar = Fuelbar(x+2.0, y-0.3, COLOR_BLACK);
}

void update_score(glm::mat4 VP)
{
    int number = score;
    int units = score%10;
    int tens_temp = score/10;
    int tens = tens_temp%10;
    int hundreds = tens_temp/10;
    segments_score[0].draw(VP, hundreds+'0');
    segments_score[1].draw(VP, tens+'0');
    segments_score[2].draw(VP, units+'0');
}

void update_speed(glm::mat4 VP)
{
    int number = speedtext;
    int units = speedtext%10;
    int tens_temp = speedtext/10;
    int tens = tens_temp%10;
    int hundreds = tens_temp/10;
    segments_speed[0].draw(VP, hundreds+'0');
    segments_speed[1].draw(VP, tens+'0');
    segments_speed[2].draw(VP, units+'0');
}

void update_height(glm::mat4 VP)
{
    int number = altitude;
    int units = altitude%10;
    int tens_temp = altitude/10;
    int tens = tens_temp%10;
    int hundreds = tens_temp/10;
    segments_height[0].draw(VP, hundreds+'0');
    segments_height[1].draw(VP, tens+'0');
    segments_height[2].draw(VP, units+'0');
}

void update_fuel(glm::mat4 VP)
{
    bar.draw(VP, (int)fuel);
}

glm::vec3 position;

void draw() {

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram (programID);

    u_x = 0;
    u_y = 1; 
    u_z = 0;
    
    if (cam_view==1) // plane view - p
    {
        x_a = Jet1.position.x-Jet1.z_ref1.x*2;   	
        z_a = Jet1.position.z-Jet1.z_ref1.z*2;   	
        y_a = Jet1.position.y-Jet1.z_ref1.y*2;   	

        t_x = Jet1.position.x-Jet1.z_ref1.x*10;   	
        t_z = Jet1.position.z-Jet1.z_ref1.z*10;   	
        t_y = Jet1.position.y-Jet1.z_ref1.y*10;  

        u_x = Jet1.y_ref1.x; 	
        u_y = Jet1.y_ref1.y; 	
        u_z = Jet1.y_ref1.z; 	
    }
   	else if (cam_view==2) //top-view - o
   	{
		x_a = Jet1.position.x;
		y_a = Jet1.position.y+10;
		z_a = Jet1.position.z;   	
        t_x = Jet1.position.x;   	
        t_z = Jet1.position.z-1;   	
        t_y = Jet1.position.y;
   	}
    else if (cam_view==3) //tower-view - i
    {       
		x_a = 10;
		y_a = 50;
		z_a = 0;   	
        t_x = Jet1.position.x;   	
        t_z = Jet1.position.z;   	
        t_y = Jet1.position.y;	    	   	
    }
    else if (cam_view==4) //follow-cam-view - u
    {       
		x_a = Jet1.position.x+Jet1.y_ref1.x*2+Jet1.z_ref1.x*2;
		y_a = Jet1.position.y+Jet1.y_ref1.y*2+Jet1.z_ref1.y*2;
		z_a = Jet1.position.z+Jet1.y_ref1.z*2+Jet1.z_ref1.z*2;   	
        t_x = Jet1.position.x;   	
        t_z = Jet1.position.z;   	
        t_y = Jet1.position.y;
        u_x = Jet1.y_ref1.x; 	
        u_y = Jet1.y_ref1.y; 	
        u_z = Jet1.y_ref1.z; 	
    }

    glm::vec3 eye ( x_a, y_a, z_a);

    glm::vec3 target (t_x, t_y, t_z);

    glm::vec3 up (u_x, u_y, u_z);

    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D  
    

    if (cam_view==5)
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        glfwSetCursorPos(window, 1024/2, 768/2);

        horizontalAngle += mouseSpeed * float(1024/2 - xpos );
        verticalAngle   += mouseSpeed * float( 768/2 - ypos );
        
        glm::vec3 direction(
            cos(verticalAngle) * sin(horizontalAngle), 
            sin(verticalAngle),
            cos(verticalAngle) * cos(horizontalAngle)
        );

        glm::vec3 right = glm::vec3(
            sin(horizontalAngle - 3.14f/2.0f), 
            0,
            cos(horizontalAngle - 3.14f/2.0f)
        );
        
        glm::vec3 up = glm::cross( right, direction );
        
        if (glfwSetScrollCallback(window, scroll_callback)){
            if (directionflag>0)
                position += direction * 0.01f * speed;
            else if (directionflag<0)
                position -= direction * 0.01f * speed;
            directionflag=0;
        }

        if (glfwGetKey( window, GLFW_KEY_M ) == GLFW_PRESS){
            position += right * 0.1f * speed;
        }

        if (glfwGetKey( window, GLFW_KEY_N ) == GLFW_PRESS){
            position -= right * 0.1f * speed;
        }
        
        float FoV = (float)45.0f;

        Matrices.view       = glm::lookAt(
                                    position,           
                                    position+direction, 
                                    up                  
                            );
    }

    glm::mat4 VP = Matrices.projection * Matrices.view;
    glm::mat4 VP_text = glm::ortho(-5.0f, 10.0f, -5.0f, 5.0f, 0.1f, 500.0f) * glm::lookAt( glm::vec3(0,0,1), glm::vec3(0,0,0), glm::vec3(0, 1, 0) );
    glm::mat4 MVP;  

//	glUseProgram(programID_blender);
    //BigIsland.draw(VP);
    //Jet1.draw(VP);

	/*glUseProgram(programID_blender);

    seaobject.draw(VP);
	glUseProgram(programID_blender);

	glUseProgram(programID_blender);
*/
    //jet.draw(VP);
//    glUseProgram(programID);
    //cone.draw(VP);
    //cylinder.draw(VP);
    seacuboid.draw(VP);
    for (int i=0; i<cans.size(); i++)
        cans[i].draw(VP);
    for (int i=0; i<rings.size(); i++)
        rings[i].draw(VP);
    for (int i=0; i<volcanoes.size(); i++)
        volcanoes[i].draw(VP);
    for (int i=0; i<bullets.size(); i++)
        bullets[i].draw(VP);
    for (int i=0; i<bombs.size(); i++)
        bombs[i].draw(VP);
    for (int i=0; i<missiles.size(); i++)
        missiles[i].draw(VP);
    for (int i=0; i<enemies.size(); i++)
        enemies[i].draw(VP);
    Cannon temp_cannon;
    if (cannons.size()>0)
        temp_cannon = cannons.back();
    temp_cannon.draw(VP);
    Cannongun temp_cannongun = cannon_guns.back();
    temp_cannongun.draw(VP);
    Arrow temp_carrow = cannon_arrows.back();
    temp_carrow.draw(VP);

    Jet1.draw(VP);

    //Dash Board Stuff
    arrow.draw(VP_text);
    for (int i=0; i<segments.size(); i++)
        segments[i].draw(VP_text);
    compassring1.draw(VP_text);
    compass.draw(VP_text);
    update_score(VP_text);
    update_speed(VP_text);
    update_height(VP_text);
    update_fuel(VP_text);
}

void bomb_release()
{
    bombs.push_back(Cuboid(Jet1.position.x, Jet1.position.y, Jet1.position.z, 0.5, 0.5, 0.5, COLOR_GOLD));
}
int missile_counter = 0;
void missile_launch()
{
    if (missile_counter>30)
    {
        Cylinder temp_missile = Cylinder(Jet1.position.x, Jet1.position.y, Jet1.position.z, 1, 0.1, COLOR_GOLD);
        temp_missile.align(Jet1.z_ref1);
        temp_missile.move(-1.2);
        missiles.push_back(temp_missile);
        missile_counter = 0;
    }
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
        bomb_release();
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
        missile_launch();
}

void tick_input(GLFWwindow *window) {
    missile_counter++;

    int key_p  = glfwGetKey(window, GLFW_KEY_P);
    int key_o = glfwGetKey(window, GLFW_KEY_O);
    int key_i = glfwGetKey(window, GLFW_KEY_I);
    int key_u = glfwGetKey(window, GLFW_KEY_U);
    int key_y = glfwGetKey(window, GLFW_KEY_Y);
    int yawleft = glfwGetKey(window, GLFW_KEY_A);
    int yawright = glfwGetKey(window, GLFW_KEY_D);
    int forward = glfwGetKey(window, GLFW_KEY_UP);
    int backward = glfwGetKey(window, GLFW_KEY_DOWN);
    int pitchdown = glfwGetKey(window, GLFW_KEY_S);
    int pitchup = glfwGetKey(window, GLFW_KEY_W);
    int rollleft  = glfwGetKey(window, GLFW_KEY_LEFT);
    int rollright = glfwGetKey(window, GLFW_KEY_RIGHT);

    if (glfwGetKey(window, GLFW_KEY_B))
        missile_launch();
    float newpitch = 0.0;
    float newyaw = 0.0;
    float newroll = 0.0;

    if (key_p) {
    	//Plane View
        cam_view = 1;
    }
    else if (key_o) {
    	//Top View
        cam_view = 2;
    }
    else if (key_i){
        //Tower View
        cam_view = 3;
    }
    else if (key_u){
        //Follow Cam View
        cam_view = 4;
    }
    else if (key_y){
        //Helicopter View
        position[0] = Jet1.position.x+Jet1.y_ref1.x*2+Jet1.z_ref1.x*2;
		position[1] = Jet1.position.y+Jet1.y_ref1.y*2+Jet1.z_ref1.y*2;
		position[2] = Jet1.position.z+Jet1.y_ref1.z*2+Jet1.z_ref1.z*2;   	

        cam_view = 5;
    }

    if (yawleft) {
        newyaw++;
        Jet1.rotateall(newyaw, 0.0, 0.0);
    }
    else if (yawright) {
        newyaw--;
        Jet1.rotateall(newyaw, 0.0, 0.0);    
    }
    
    if (pitchup) {
    //    printf("Pitch up detectd\n");
        newpitch++;
        Jet1.rotateall(0.0, newpitch, 0.0);    
    }
    else if (pitchdown) {
        newpitch--;
        Jet1.rotateall(0.0, newpitch, 0.0);    
    }

    if (rollleft) {
        newroll--;
        Jet1.rotateall(0.0, 0.0, newroll);    
    }
    else if (rollright) {
        newroll++;
        Jet1.rotateall(0.0, 0.0, newroll);    
    }

    if(forward == GLFW_PRESS){
         Jet1.boost(1);
    }
    else if (backward == GLFW_PRESS){
         Jet1.boost(-1);
    }

    if (glfwSetMouseButtonCallback(window, mouse_button_callback)){
    }
}

int isNearCheckpoint(){
    Cannon nextcannon = cannons.back();
    return abs(nextcannon.position.x-Jet1.position.x)<30&&
            abs(nextcannon.position.z-Jet1.position.z)<30;
}

void tick_elements() {
    Jet1.tick();
    glm::vec3 temp_compass = glm::vec3(Jet1.z_ref1.x,1*Jet1.z_ref1.z,0);
    temp_compass = glm::normalize(temp_compass);
    compass.align(temp_compass);

    if (cannons.size()>0)
    {
        Arrow carrow = cannon_arrows.back();
        Cannongun cgun = cannon_guns.back();
        glm::vec3 thevector = glm::vec3(Jet1.position) - cgun.position;
        thevector = glm::normalize(thevector);
        cgun.align(thevector);
        

        if (isNearCheckpoint())
        {
            bullet_counter++;
            if (bullet_counter==50)
            {
                bullet_counter=0;
                Cuboid temp_bullet = Cuboid(cgun.position.x+cgun.z_ref1.x*4, cgun.position.y+cgun.z_ref1.y*4, cgun.position.z+cgun.z_ref1.z*4, 0.3, 0.3, 0.3, COLOR_BLACK);
                temp_bullet.z_ref1 = thevector;
                bullets.push_back(temp_bullet);
            }
        }

        glm::vec3 arrow_aligner;
        arrow_aligner = glm::vec3(cgun.position.x-Jet1.position.x,
                                cgun.position.y-Jet1.position.y-Jet1.y_ref1.y*4,
                                cgun.position.z-Jet1.position.z
                            );
        arrow_aligner = glm::normalize(arrow_aligner);
        //arrow_aligner = arrow_aligner - glm::normalize(Jet1.z_ref1);
        //arrow_aligner = glm::normalize(arrow_aligner);
        arrow.align(arrow_aligner);
    }

    for (int i=0; i<bombs.size(); i++)
    {
        if (bombs[i].tick())
            bombs.erase(bombs.begin()+i);
    }
    for (int i=0; i<missiles.size(); i++)
    {
        if (missiles[i].tick())
            missiles.erase(missiles.begin()+i);
    }
    for (int i=0; i<enemies.size(); i++)
    {
        if (enemies[i].tick())
            enemies.erase(enemies.begin()+i);
    }
    for (int i=0; i<bullets.size(); i++)
    {
        if (bullets[i].bullet_tick())
            bullets.erase(bullets.begin()+i);
    }
    arrow.tick();
}

void world_creator(){

    for (int i=1; i<20; i++)    
    {
        cans.push_back(Cuboid(rand()%100-50, altitude, -50*i, 1, 2, 1, COLOR_GREEN));
        cans.push_back(Cuboid(50*i, altitude, rand()%100-5, 1, 2, 1, COLOR_GREEN));
    }
    
    rings.push_back(Ring(1, altitude, -20, 2, COLOR_FIRE));
    rings.push_back(Ring(1, altitude, -20, 1.5, COLOR_BACKGROUND));

    rings.push_back(Ring(-3, altitude, -30, 2, COLOR_FIRE));
    rings.push_back(Ring(-3, altitude, -30, 1.5, COLOR_BACKGROUND));

    rings.push_back(Ring(-5, altitude,  20, 2, COLOR_FIRE));
    rings.push_back(Ring(-5, altitude,  20, 1.5, COLOR_BACKGROUND));

    rings.push_back(Ring(4, altitude, -30, 2, COLOR_FIRE));
    rings.push_back(Ring(4, altitude, -30, 1.5, COLOR_BACKGROUND));

    volcanoes.push_back(Volcano(1, 1, -45));
    volcanoes.push_back(Volcano(20, 1, -35));
    volcanoes.push_back(Volcano(33, 1, -55));
    volcanoes.push_back(Volcano(45, 1, 50));
    volcanoes.push_back(Volcano(59, 1, -45));
    volcanoes.push_back(Volcano(60, 1, -65));
    for (int i=0; i<20; i++)
        volcanoes.push_back(Volcano(60+rand()%10*i, 1, -1 * (65 + rand()%20 * i)));

    enemies.push_back(Enemy(-5, altitude+5, -5));
    enemies.push_back(Enemy(5, altitude+20, -20));

    /*enemies.push_back(Enemy(10, altitude+20, -25));
    enemies.push_back(Enemy(20, altitude+20, -35));
    enemies.push_back(Enemy(30, altitude+20, -30));
    enemies.push_back(Enemy(25, altitude+20, -40));
    enemies.push_back(Enemy(-20, altitude+20, -45));
    enemies.push_back(Enemy(-25, altitude+20, -50));
    enemies.push_back(Enemy(35, altitude+20, -55));
    enemies.push_back(Enemy(-45, altitude+20, -60));
    enemies.push_back(Enemy(55, altitude+20, -65));
    enemies.push_back(Enemy(65, altitude+20, -70));*/
    

    Arrow temp_Arrow;

    cannons.push_back(Cannon(75, 0, -50, 1));
    cannon_guns.push_back(Cannongun(75, 4, -50, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(75, altitude, -50, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(125, 0, -65, 1));
    cannon_guns.push_back(Cannongun(125, 4, -65, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(125, altitude, -65, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(125, 0,  75, 1));
    cannon_guns.push_back(Cannongun(125, 4, 75, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(125, altitude, 75, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(-125, 0,  75, 1));
    cannon_guns.push_back(Cannongun(-125, 4, 75, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(-125, altitude, 75, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(250, 0,  -75, 1));
    cannon_guns.push_back(Cannongun(250, 4, -75, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(250, altitude, -75, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(-225, 0,  35, 1));
    cannon_guns.push_back(Cannongun(-225, 4, 35, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(-225, altitude, 35, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(245, 0,  -45, 1));
    cannon_guns.push_back(Cannongun(245, 4, -45, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(245, altitude, -45, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(50, 0, -30, 1));
    cannon_guns.push_back(Cannongun(50, 4, -30, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(50, 20, -30, 4, 2, 0.4, COLOR_ORANGE);
    cannon_arrows.push_back(temp_Arrow);

    cannons.push_back(Cannon(100, 0, -30, 1));
    cannon_guns.push_back(Cannongun(100, 4, -30, 0.4, 4, 0.4, COLOR_GOLD));
    temp_Arrow = Arrow(100, altitude, -30, 4, 2, 0.4, COLOR_PURERED);
    cannon_arrows.push_back(temp_Arrow);
}

void collision_detector(){
    for (int i=0; i<cans.size(); i++)
    {
        if (detect_collision(Jet1.bounding_box(), cans[i].bounding_box()))
        {
            score += 10;
            cans.erase(cans.begin()+i);
            fuel = 100;
        }
    }
    for (int i=0; i<rings.size(); i++)
    {
        if (detect_collision(Jet1.bounding_box(), rings[i].bounding_box()))
        {
            score += 20;
            rings.erase(rings.begin()+i);
        }
    }
    for (int i=0; i<volcanoes.size(); i++)
    {
        if (detect_collision(Jet1.bounding_box(), volcanoes[i].bounding_box()))
        {
            score += 0;
            exit(0);
        }
    }
    for (int i=0; i<enemies.size(); i++)
    {
        //printf("Enemy %d, is at %f, %f, %f\n", i, enemies[i].position.x, enemies[i].position.y, enemies[i].position.z);
        for (int j=0; j<missiles.size(); j++)
        {
            //printf("Missile %d, is at %f, %f, %f\n", j, missiles[j].position.x, missiles[j].position.y, missiles[j].position.z);
            bounding_box_t a = enemies[i].bounding_box();
            bounding_box_t b = missiles[j].bounding_box();
            //printf("a.x: %f, a.y: %f. a.z: %f\nb.x: %f, b.y: %f. b.z: %f\n", a.x, a.y, a.z, b.x, b.y, b.z);
            //printf("a.x_length: %f, a.y_length: %f. a.z_length: %f\nb.x_length: %f, b.y_length: %f. b.z_length: %f\n", a.x_length, a.y_length, a.z_length, b.x_length, b.y_length, b.z_length);
            if (detect_collision(enemies[i].bounding_box(), missiles[j].bounding_box()))
            {
                score += 50;
                enemies.erase(enemies.begin()+i);
                missiles.erase(missiles.begin()+j);
            }
        }
    }
    for (int i=0; i<enemies.size(); i++)
    {
        for (int j=0; j<bombs.size(); j++)
        {
            if (detect_collision(enemies[i].bounding_box(), bombs[j].bounding_box()))
            {
                score += 50;
                enemies.erase(enemies.begin()+i);
                bombs.erase(bombs.begin()+j);
            }
        }
    }
    Cannon temp_check;
    if (cannons.size()>0)
        temp_check = cannons.back();
    int bombed = 0;
    for (int j=0; j<missiles.size(); j++)
    {
        if (detect_collision(temp_check.bounding_box(), missiles[j].bounding_box()))
        {
            score += 50;
            bombed = 1;
            cannons.pop_back();
            cannon_guns.pop_back();
            cannon_arrows.pop_back();
        }
    }
    if ((bombed==0)&&(cannons.size()>0))
    {    
        for (int j=0; j<bombs.size(); j++)
        {
            if (detect_collision(temp_check.bounding_box(), bombs[j].bounding_box()))
            {
                score += 50;
                cannons.pop_back();
                cannon_guns.pop_back();
                cannon_arrows.pop_back();
            }
        }
    }
    bombed = 0;
    for (int i=0; i<bullets.size(); i++)
    {
        if (detect_collision(Jet1.bounding_box(), bullets[i].bounding_box()))
        {
            score = 0;
            bullets.erase(bullets.begin()+i);
        }
    }
}

void initGL(GLFWwindow *window, int width, int height) {

	programID_blender = LoadShaders( "TransformVertexShader.vertexshader", "TextureFragmentShader.fragmentshader" );
    seacuboid = Cuboid(-10, 0, 10, 1000, 0.0001, 1000, COLOR_BLUE);
    altitude = 40;
    Jet1       = Plane(0, altitude, 0, COLOR_AQUA);
    world_creator();

    arrow = Arrow(2, 4, -6, 1, 0.8, 0.1, COLOR_PURERED);
    compassring1 = Ring(8.2, -3.5, -5, 1.2, COLOR_CREAM);
    compass = Arrow2D(8.2, -3.5, -5, 0.8, 0.6, 0.0, COLOR_BLACK);
    
    init_score(-5.0f, -4.0f);
    init_speed(-5.0f, -3.5f);
    init_altitude(-5.0f, -3.0f);
    init_fuel(-5.0f, -2.5f);
    glfwSwapBuffers(window);
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");

    Matrices.MatrixID = glGetUniformLocation(programID_blender, "MVP");

    reshapeWindow (window, width, height);

    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}


int main(int argc, char **argv) {
    srand(time(0));
    int width  = 1000;
    int height = 768;

    window = initGLFW(width, height);

    initGL (window, width, height);

    while (!glfwWindowShouldClose(window)) {

        if (t60.processTick()) {
            speedtext = (int)Jet1.speed+100;
            fuel += (float)Jet1.speed/10;
            if ((fuel==0)||(altitude==0))
                exit(0);
            //printf("Height : %f\n", Jet1.position.y);
            altitude = Jet1.position.y;
            collision_detector();
            draw();
            glfwSwapBuffers(window);

            tick_elements();
            tick_input(window);
        }

        glfwPollEvents();
    }
    glDeleteProgram(programID_blender);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    if ((b.isMissile)&&((a.type==0)))
    {
        //printf("a.x: %f, a.y: %f. a.z: %f\nb.x: %f, b.y: %f. b.z: %f\n", a.x, a.y, a.z, b.x, b.y, b.z);
        //printf("a.x_length: %f, a.y_length: %f. a.z_length: %f\nb.radius: %f\n", a.x_length, a.y_length, a.z_length, b.radius);
        //Fuel Can & Plane
        return ((abs(a.x - b.x) < 5) &&
               (abs(a.y - b.y) < 5) &&
               (abs(a.z - b.z) < 5));
    }
    if ((b.type==0)&&((a.type==-1)||(a.type==0)))
    {
        //Fuel Can & Plane
        return (abs(a.x - b.x) < max(a.x_length,b.x_length)) &&
               (abs(a.y - b.y) < max(a.y_length,b.y_length)) &&
               (abs(a.z - b.z) < max(a.z_length,b.z_length));
    }
    else if ((b.type==2)&&(a.type==-1))
    {
        //Inside the ring & Plane
        //printf("a.x: %f, a.y: %f. a.z: %f\nb.x: %f, b.y: %f. b.z: %f\n", a.x, a.y, a.z, b.x, b.y, b.z);
        //printf("a.x_length: %f, a.y_length: %f. a.z_length: %f\nb.radius: %f\n", a.x_length, a.y_length, a.z_length, b.radius);
        return ((b.z>=a.z+1.5) &&
               (b.x-b.radius<a.x&&b.x+b.radius>a.x+a.x_length) &&
               (b.y-b.radius<a.y&&b.y+b.radius>a.y+a.y_length));
    }
    else if ((b.type==3)&&(a.type==-1))
    {
        //Above the volcano & Plane
        //printf("a.x: %f, a.y: %f. a.z: %f\nb.x: %f, b.y: %f. b.z: %f\n", a.x, a.y, a.z, b.x, b.y, b.z);
        //printf("a.x_length: %f, a.y_length: %f. a.z_length: %f\nb.radius: %f b.length:%f\n", a.x_length, a.y_length, a.z_length, b.radius, b.length);
        return ((a.y<b.y+b.length+20) &&
               (b.x-b.radius<a.x&&b.x+b.radius>a.x+a.x_length) &&
               (b.z-b.radius<a.z&&b.z+b.radius>a.z+a.z_length));
    }
}

void reset_screen() {
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 4 / screen_zoom;
    float right  = screen_center_x + 4 / screen_zoom;
    Matrices.projection = glm::perspective(float(M_PI_2), 1.0f, 0.1f, 500.0f);

}
