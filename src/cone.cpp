#include <vector>
#include <cmath>
#include <utility>
#include "cone.h"
#include "main.h"

using namespace std;

Cone::Cone(float x, float y, float z, float length, float radius, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 270;
    this->length = length;
    this->radius = radius;
    acc = 0; speed=0.01;
    const int n=500;
    GLfloat vertex_buffer_data[9*n];
    for (int i=0;i<n;++i){
        vertex_buffer_data[9*i]=(radius*cos(i*2*M_PI/n));
        vertex_buffer_data[9*i+1]=(radius*sin(i*2*M_PI/n));
        vertex_buffer_data[9*i+2]=0.0;

        vertex_buffer_data[9*i+3]=(radius*cos((i+1)*2*M_PI/n));
        vertex_buffer_data[9*i+4]=(radius*sin((i+1)*2*M_PI/n));
        vertex_buffer_data[9*i+5]=0.0;

        vertex_buffer_data[9*i+6]=0.0;
        vertex_buffer_data[9*i+7]=0.0;
        vertex_buffer_data[9*i+8]=length;
    }

    this->object = create3DObject(GL_TRIANGLES, 3*n, vertex_buffer_data, color, GL_FILL);
}

void Cone::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (glm::vec3(position.x, position.y, position.z));    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    Matrices.model *= (translate * rotate );
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Cone::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

int Cone::tick() {
    speed += acc;
    position.y -= speed;
    if (position.y<=0)
        return 1;
    return 0;
}

