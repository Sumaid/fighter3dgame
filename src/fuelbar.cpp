#include <vector>
#include "line.h"
#include "circle.h"
#include "fuelbar.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

Fuelbar::Fuelbar(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->speed = 0.01;
    thick = 0.03f;
    float length = 0.3;
    a = Rect(x, y, 0, length, length, color);
    b = Rect(x+length, y, 0, length, length, color);
    c = Rect(x+2*length, y, 0, length, length, color);
    d = Rect(x+3*length, y, 0, length, length, color);
    e = Rect(x+4*length, y, 0, length, length, color);
    f = Rect(x+5*length, y, 0, length, length, color);
}

void Fuelbar::draw(glm::mat4 VP, int input) {
    if (input>5*100/6)
    {
        f.draw(VP);
    }    
    if (input>4*100/6)
    {
        e.draw(VP);
    }
    if (input>3*100/6)
    {
        d.draw(VP);
    }
    if (input>2*100/6)
    {
        c.draw(VP);
    }
    if (input>1*100/6)
    {
        b.draw(VP);
    }
    if (input>0*100/6)
    {
        a.draw(VP);
    }    
}

void Fuelbar::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}
