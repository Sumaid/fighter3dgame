#include "main.h"
#include <vector>

#ifndef JET_H
#define JET_H


class Jet {
public:
    Jet() {}
    Jet(float x, float y, float z, color_t color);
    glm::vec3 x_ref, y_ref, z_ref, z_ref1;
    glm::vec4 position;
    glm::mat4 rotation;
	GLuint vertexbuffer;
	GLuint uvbuffer, normalbuffer;
    GLuint programID, MatrixID;
	GLuint VertexArrayID;
    GLuint Texture;
    GLuint TextureID; 
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals; // Won't be used at the moment.
    glm::vec3 gOrientation1;
    bounding_box_t bounding_box();
    float pitch, roll, yaw;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    void tick();
    void boost(int flag);
    void rotateall(float yaw, float pitch, float roll);
    double speed;
    float acceleration;
private:
    VAO *object;
};

#endif // Jet_H
