#include "main.h"

#ifndef CONE_H
#define CONE_H


class Cone {
public:
    Cone() {}
    Cone(float x, float y, float z, float length, float radius, color_t color);
    glm::vec3 position;
    float rotation, acc, speed;
    float length, radius;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    int tick();
private:
    VAO *object, *object1;
};

#endif // Cone_H
