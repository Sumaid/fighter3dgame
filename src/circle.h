#include "main.h"

#ifndef CIRCLE_H
#define CIRCLE_H


class Circle {
public:
    Circle() {}
    Circle(float x, float y, float length, color_t color);
    glm::vec3 position;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y);
    int tick(float flag);
    int lefttick(float flag);
    int move(float flag);
    double speed;
    float length;
    bounding_box_t bounding_box();
private:
    VAO *circle;
};

#endif // CIRCLE_H
