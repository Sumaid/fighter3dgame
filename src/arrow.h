#include "main.h"
#include "cylinder.h"
#include "cone.h"
#include <vector>

#ifndef ARROW_H
#define ARROW_H


class Arrow {
public:
    Arrow() {}
    Arrow(float x, float y, float z, float vlength, float hlength, float dlength, color_t color);
    glm::vec3 x_ref, y_ref, z_ref, z_ref1, y_ref1;
    glm::vec4 position;
    glm::mat4 rotation;
    int drawflag = 0;
    float pitch, roll, yaw;
    void draw(glm::mat4 VP);
    float scalingfactor;
    void set_position(float x, float y, float z);
    void tick();
    void align(glm::vec3 axis);
    void boost(int flag);
    void rotateall(float yaw, float pitch, float roll);
    double speed;
    float acceleration;
    bounding_box_t bounding_box();
private:
    VAO *object, *object1;
};

#endif // Jet_H
