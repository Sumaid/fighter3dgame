#include <vector>
#include "line.h"
#include "circle.h"
#include "seg7.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

Segment::Segment(float x, float y, char input) {
    this->position = glm::vec3(x, y, 0);
    this->speed = 0.01;
    thick = 0.03f;
    float length = 0.3;

    if (input=='N')
    {
        lines.push_back(Line(x, y, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length/2, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length/2, y-length/2, x+length, y-length, thick, COLOR_BLACK));
    }
    if (input=='S')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length, x, y-length, thick, COLOR_BLACK));
    }
    else if (input=='T')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x+length/2, y, x+length/2, y-length, thick, COLOR_BLACK));
    }
    else if (input=='A')
    {
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='G')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length/2, y-length/2, thick, COLOR_BLACK));        
    }
    else if (input=='E')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length/2, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='F')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length/2, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='C')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='R')
    {
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='q')
    {
        circles.push_back(Circle(x+length/2, y-0.05, 0.05, COLOR_BLACK));
        circles.push_back(Circle(x+length/2, y-0.25, 0.05, COLOR_BLACK));
    }
    else if (input=='0')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='U')
    {
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='L')
    {
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='1')
    {
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BACKGROUND));
    }
    else if (input=='2')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length, y-length, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x+length, y-length/2, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x, y, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length/2, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
    }
    else if (input=='3')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BACKGROUND));
    }
    else if (input=='4')
    {
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length/2, x, y-length, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BACKGROUND));
    }
    else if (input=='5')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length/2, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length, x, y-length/2, thick, COLOR_BACKGROUND));
    }
    else if (input=='6')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y-length, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BACKGROUND));
    }
    else if (input=='7')
    {
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length/2, x+3, y-length/2, thick, COLOR_BACKGROUND));
        lines.push_back(Line(x, y-length, x+3, y-length, thick, COLOR_BACKGROUND));
    }
    else if (input=='9')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x, y-length, thick, COLOR_BACKGROUND));
    }
    else if (input=='8')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='P')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length/2, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='H')
    {
        lines.push_back(Line(x, y, x, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length, y, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length/2, x+length, y-length/2, thick, COLOR_BLACK));
    }
    else if (input=='I')
    {
        lines.push_back(Line(x, y, x+length, y, thick, COLOR_BLACK));
        lines.push_back(Line(x, y-length, x+length, y-length, thick, COLOR_BLACK));
        lines.push_back(Line(x+length/2, y, x+length/2, y-length, thick, COLOR_BLACK));
    }
}

void Segment::refresh(){
    lines.clear();
    circles.clear();
}

void Segment::draw(glm::mat4 VP) {
    for (int i=0; i<lines.size(); i++)
        lines[i].draw(VP);
    for (int i=0; i<circles.size(); i++)
        circles[i].draw(VP);
}

void Segment::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

int Segment::tick(float flag) {
    for (int i=0; i<lines.size(); i++)
        lines[i].tick(0.1, 0);
}
