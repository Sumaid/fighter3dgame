#define GLM_ENABLE_EXPERIMENTAL
#include "arrow2d.h"
#include "main.h"

using namespace glm;

Arrow2D::Arrow2D(float x, float y, float z, float vlength, float hlength, float dlength, color_t color) {
    this->position = glm::vec4(x, y, z, 0);
    this->x_ref = glm::vec3(1,0,0);
    this->y_ref = glm::vec3(0,0,1);
    this->z_ref = glm::vec3(0,1,0);
    this->rotation_angle = 180;
    drawflag = 0;
    this->rotation = glm::rotate((float) (this->rotation_angle * M_PI / 180.0f), this->y_ref);
    this->pitch = 0; this->roll = 0; this->yaw = 0;
    this->z_ref1 = glm::vec3(0,1,0);
    this->vlength = vlength;
    this->hlength = hlength;
    this->dlength = dlength;


    GLfloat vertex_buffer_data[] = {
        0.0, vlength, -dlength/2,
       -hlength/2, vlength/2, -dlength/2,
        hlength/2, vlength/2, -dlength/2,
        -hlength/5, vlength/2, -dlength/2, 
        hlength/5, vlength/2, -dlength/2,
        -hlength/5, 0.0, -dlength/2,
        hlength/5, vlength/2, -dlength/2,
        -hlength/5, 0.0, -dlength/2,
        hlength/5, 0.0, -dlength/2,
        0.0, vlength,  dlength/2,
       -hlength/2, vlength/2, dlength/2,
        hlength/2, vlength/2, dlength/2,
        -hlength/5, vlength/2, dlength/2, 
        hlength/5, vlength/2, dlength/2,
        -hlength/5, 0.0, dlength/2,
        hlength/5, vlength/2, dlength/2,
        -hlength/5, 0.0, dlength/2,
        hlength/5, 0.0, dlength/2
    };  
    this->object = create3DObject(GL_TRIANGLES, 9*2, vertex_buffer_data, color, GL_FILL);
}

void Arrow2D::align(glm::vec3 axis){
    z_ref1 = axis;
//	printf("axis.x: %f, axis.y: %f, axis.z:%f\n", axis.x, axis.y, axis.z);
//	z_ref1 = axis;
    //glm::vec3 axis = glm::vec3(axis.x*3, axis.y*3, axis.z*3);
	//glm::vec3 temp = axis + glm::vec3(1,1,1);
	//glm::vec3 temp_x = glm::normalize(temp);
	glm::vec3 temp_x = glm::normalize(glm::cross(axis, glm::vec3(0,0,1)));

	rotation[0] = glm::vec4(temp_x,0.0f);
	rotation[1] = glm::vec4(axis,0.0f);
	rotation[2] = glm::vec4(glm::vec3(0,0,1),0.0f);
	rotation[3] = glm::vec4(0.0f,0.0f,0.0f,1.0f);
}

void Arrow2D::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (vec3(this->position.x, this->position.y, this->position.z));    // glTranslatef
    glm::mat4 myScalingMatrix;
    float scalingfactor = 1.0f;
    myScalingMatrix[0] = vec4(scalingfactor, 0.0, 0.0, 0.0);
    myScalingMatrix[1] = vec4(0.0, scalingfactor, 0.0, 0.0);
    myScalingMatrix[2] = vec4(0.0, 0.0, scalingfactor, 0.0);
    myScalingMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0f);

    Matrices.model *= (translate * rotation * myScalingMatrix);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    drawflag++;
    draw3DObject(this->object);
}

void Arrow2D::set_position(float x, float y, float z) {
    this->position = glm::vec4(x, y, z, 0);
}

void Arrow2D::boost(int flag){
//    if (flag>0)
//        speed += acceleration;
//    else 
//        speed -= acceleration;
    position.x  += z_ref1[0]*speed;
    position.y  += z_ref1[1]*speed;
    position.z  += z_ref1[2]*speed;

}

void Arrow2D::tick() {
    //printf("x:%f, y:%f, z:%f\n", position.x, position.y, position.z);
    //position.x  += z_ref1[0]*speed;
    //position.y  += z_ref1[1]*speed;
    //position.z  += z_ref1[2]*speed;
    //this->rotation += speed;
    // this->position.x -= speed;
    // this->position.y -= speed;
}

bounding_box_t Arrow2D::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = -1;
    box.x = position.x-0.2; box.y = position.y-0.2; box.z = position.z-1;
    box.x_length = 0.4; box.y_length = 0.4; box.z_length = 2.0;
    return box;
}