#include <cmath>
#include <vector>
#include <utility>
#include "ring.h"

using namespace std; 

Ring::Ring(float x, float y, float z, float radius, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    speed = 0.01;
    this->radius = radius;
    rotate = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(0, 0, 1));
    const int n=500;
    GLfloat vertex_buffer_data[9*n];
    for (int i=0;i<n;++i){
        vertex_buffer_data[9*i]=(radius*cos(i*2*M_PI/n));
        vertex_buffer_data[9*i+1]=(radius*sin(i*2*M_PI/n));
        vertex_buffer_data[9*i+2]=0.0;

        vertex_buffer_data[9*i+3]=(radius*cos((i+1)*2*M_PI/n));
        vertex_buffer_data[9*i+4]=(radius*sin((i+1)*2*M_PI/n));
        vertex_buffer_data[9*i+5]=0.0;

        vertex_buffer_data[9*i+6]=0.0;
        vertex_buffer_data[9*i+7]=0.0;
        vertex_buffer_data[9*i+8]=0.0;
    }

    this->ring = create3DObject(GL_TRIANGLES, 3*n, vertex_buffer_data, color, GL_FILL);
}

void Ring::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    translate = glm::translate (this->position);    // glTranslatef
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->ring);
}

void Ring::rotate_matrix(float angle, glm::vec3 axis)
{
    rotate = glm::rotate((float) (angle * M_PI / 180.0f), axis);
}

void Ring::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

bounding_box_t Ring::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = 2;
    box.x = x; box.y = y; box.z = z;
    box.radius = 1.0;
    return box;
}
