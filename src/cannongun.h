#include "main.h"

#ifndef CANNONGUN_H
#define CANNONGUN_H


class Cannongun {
public:
    Cannongun() {}
    Cannongun(float x, float y, float z, float x_length, float y_length, float z_length, color_t color);
    glm::vec3 position, z_ref1;
void align(glm::vec3 axis);
    float rotation, acc, speed;
    glm::mat4 rotation_matrix;
    float x_length, y_length, z_length;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    int tick();
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // CANNONGUN_H
