#include <vector>
#include "cuboid.h"
#include "cylinder.h"
#include "cone.h"
#include "cannon.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

Cannon::Cannon(float x, float y, float z, int index) {
    this->index = index;
    this->position = glm::vec3(x, y, z);
    upperbase = Cuboid(x, y+1, z, 3, 1, 3, COLOR_ARMY);
    lowerbase = Cuboid(x, y, z, 6, 1, 6, COLOR_BROWN);
}

void Cannon::draw(glm::mat4 VP) {
    upperbase.draw(VP);
    lowerbase.draw(VP);
}

void Cannon::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

bounding_box_t Cannon::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = 0;
    box.x = x-1; box.y = y-0.15; box.z = z-1;
    box.x_length = 6; 
    box.y_length = 2;
    box.z_length = 6;
    return box;
}

void Cannon::rotate_matrix(glm::vec3 axis){
    //gun.align(axis);
}
