#include <vector>
#include <cmath>
#include <utility>
#include "cylinder.h"
#include "main.h"

using namespace std;

Cylinder::Cylinder(float x, float y, float z, float length, float radius, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    this->length = length;
    this->radius = radius;
	this->speed = 0.5;
    int n = 100;
    GLfloat vertex_buffer_data[27*n];
    rotation_matrix    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));

	for(int i=0; i<27*n; i+=27){ 
		vertex_buffer_data[i]    = (float)radius*cos(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+1]  = (float)radius*sin(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+2]  = 0.0f;
		vertex_buffer_data[i+3]  = (float)radius*cos(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+4]  = (float)radius*sin(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+5]  = 0.0f;
		vertex_buffer_data[i+6]  = (float)radius*cos(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+7]  = (float)radius*sin(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+8]  = length;
		vertex_buffer_data[i+9]  = (float)radius*cos(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+10] = (float)radius*sin(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+11] = length;
		vertex_buffer_data[i+12] = (float)radius*cos(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+13] = (float)radius*sin(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+14] = length;
		vertex_buffer_data[i+15] = (float)radius*cos(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+16] = (float)radius*sin(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+17] = 0.0f;
		vertex_buffer_data[i+18] = (float)radius*cos(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+19] = (float)radius*sin(((i/27))*2*M_PI/n);
		vertex_buffer_data[i+20] = 0.0f;
		vertex_buffer_data[i+21] = (float)radius*cos(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+22] = (float)radius*sin(((i/27)+1)*2*M_PI/n);
		vertex_buffer_data[i+23] = 0.0f;
		vertex_buffer_data[i+24] = 0.0f;
		vertex_buffer_data[i+25] = 0.0f;
		vertex_buffer_data[i+26] = 0.0f;
	}
    this->object = create3DObject(GL_TRIANGLES, n*9, vertex_buffer_data, color, GL_FILL);
	//    this->object = create3DObject(GL_TRIANGLES, 3*n, vertex_buffer_data, color, GL_FILL);
}

void Cylinder::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    translate = glm::translate (this->position);    // glTranslatef
    Matrices.model *= (translate * rotation_matrix );
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Cylinder::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

void Cylinder::align(glm::vec3 axis){
//	printf("axis.x: %f, axis.y: %f, axis.z:%f\n", axis.x, axis.y, axis.z);
	z_ref1 = axis;

	glm::vec3 temp = axis + glm::vec3(1,1,1);
	glm::vec3 temp_x = glm::normalize(temp);
	 temp_x = glm::normalize(glm::cross(axis, temp_x));
	glm::vec3 temp_z = glm::normalize(glm::cross(temp_x, axis));

	rotation_matrix[0] = glm::vec4(temp_x,0.0f);
	x_ref1 = temp_x;
	rotation_matrix[2] = glm::vec4(axis,0.0f);
	rotation_matrix[1] = glm::vec4(temp_z,0.0f);
	y_ref1 = temp_z;
	rotation_matrix[3] = glm::vec4(0.0f,0.0f,0.0f,1.0f);
}

void Cylinder::move(float amount){
	this->position.x += z_ref1.x*amount;
	this->position.y += z_ref1.y*amount;
	this->position.z += z_ref1.z*amount;
}

int Cylinder::tick() {
    position.x  -= z_ref1[0]*speed;
    position.y  -= z_ref1[1]*speed;
    position.z  -= z_ref1[2]*speed;
    if (position.y<=0)
        return 1;
	else if ((position.x>10000)||(position.z>10000))
		return 1;
    return 0;
}

bounding_box_t Cylinder::bounding_box() {
    float x = position.x, y = position.y;
    float z = position.z;
    bounding_box_t box;
    box.type = 0;
	box.isMissile = true;
    box.x = x-radius; box.y = y-length; box.z = z-radius;
    box.x_length = radius; 
    box.y_length = length;
    box.z_length = radius;
    box.z_ref = z_ref1;
    box.x_ref = x_ref1;
    box.y_ref = y_ref1;
    return box;
}
