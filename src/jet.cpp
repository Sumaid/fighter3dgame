#define GLM_ENABLE_EXPERIMENTAL
#include "jet.h"
#include "objloader.hpp"
#include "texture.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

using namespace glm;

Jet::Jet(float x, float y, float z, color_t color) {
    this->position = glm::vec4(x, y, z, 0);
    this->rotation = mat4(1.0f);
    this->pitch = 0; this->roll = 0; this->yaw = 0;
    this->x_ref = glm::vec3(1,0,0);
    this->y_ref = glm::vec3(0,1,0);
    this->z_ref = glm::vec3(0,0,1);
    this->z_ref1 = glm::vec3(0,0,1);
    acceleration = 0.01;
    speed = 0.1f;
	//glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	//glDepthFunc(GL_LESS); 
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);

	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	MatrixID = glGetUniformLocation(programID_blender, "MVP");

	// Load the texture
	//Texture = loadDDS("da3f9212.dds");
	//Texture = loadBMP_custom("camo.bmp");

	// Get a handle for our "myTextureSampler" uniform
	//TextureID  = glGetUniformLocation(programID_blender, "myTextureSampler");

	// Read our .obj file
	bool res = loadOBJ("T-501.obj", vertices, uvs, normals);

	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
        0,                  // attribute
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glVertexAttribPointer(
        1,                                // attribute
        2,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
    glVertexAttribPointer(
        2,                                // attribute
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*)0                          // array buffer offset
    );
}

void Jet::rotateall(float yaw1, float pitch1, float roll1){
    glm::mat4 yaw_mat    = glm::rotate((float) (yaw1 * M_PI / 180.0f), this->z_ref);
    glm::mat4 pitch_mat    = glm::rotate((float) (pitch1 * M_PI / 180.0f), this->x_ref);
    glm::mat4 roll_mat    = glm::rotate((float) (roll1 * M_PI / 180.0f), this->y_ref);
    rotation *= yaw_mat*pitch_mat*roll_mat;
    this->z_ref1 = rotation[2];
}

void Jet::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (vec3(this->position.x, this->position.y, this->position.z));    // glTranslatef
    glm::mat4 myScalingMatrix;
    float scalingfactor = 0.2f;
    myScalingMatrix[0] = vec4(scalingfactor, 0.0, 0.0, 0.0);
    myScalingMatrix[1] = vec4(0.0, scalingfactor, 0.0, 0.0);
    myScalingMatrix[2] = vec4(0.0, 0.0, scalingfactor, 0.0);
    myScalingMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0f);
    
    Matrices.model *= (translate * rotation * myScalingMatrix);
    //glActiveTexture(GL_TEXTURE0);
    //glBindTexture(GL_TEXTURE_2D, Texture);
    //glUniform1i(TextureID, 0);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
    glBindVertexArray (vertexbuffer);

    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);

    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);

    glDrawArrays(GL_TRIANGLES, 0, vertices.size() );
    //glBindTexture(GL_TEXTURE_2D, 0);
}

void Jet::set_position(float x, float y, float z) {
    this->position = glm::vec4(x, y, z, 0);
}

void Jet::boost(int flag){
//    if (flag>0)
//        speed += acceleration;
//    else 
//        speed -= acceleration;
    position.x  += z_ref1[0]*speed;
    position.y  += z_ref1[1]*speed;
    position.z  += z_ref1[2]*speed;

}

void Jet::tick() {
    //printf("x:%f, y:%f, z:%f\n", position.x, position.y, position.z);
    //position.x  += z_ref1[0]*speed;
    //position.y  += z_ref1[1]*speed;
    //position.z  += z_ref1[2]*speed;
    //this->rotation += speed;
    // this->position.x -= speed;
    // this->position.y -= speed;
}

bounding_box_t Jet::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = -1;
    box.x = position.x-0.2; box.y = position.y-0.2; box.z = position.z-1;
    box.x_length = 0.4; box.y_length = 0.4; box.z_length = 2.0;
    return box;
}