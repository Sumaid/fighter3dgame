#include "main.h"
#include "line.h"
#include "cuboid.h"
#include "cone.h"
#include "rectangle.h"

#ifndef CANNON_H
#define CANNON_H

using namespace std;

class Cannon {
public:
    Cannon() {}
    Cannon(float x, float y, float z, int index);
    glm::vec3 position;
    int index;
    float rotation;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    glm::mat4 translate, rotate;
    void rotate_matrix(glm::vec3 axis);
    float speed;
    float baseradius;
    float mountain_radius;
    float mountain_length;
    Cuboid upperbase, lowerbase;
    void tick();
    bounding_box_t bounding_box();
private:
};

#endif // Cannon_H
