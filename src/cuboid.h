#include "main.h"

#ifndef CUBOID_H
#define CUBOID_H


class Cuboid {
public:
    Cuboid() {}
    Cuboid(float x, float y, float z, float x_length, float y_length, float z_length, color_t color);
    glm::vec3 position, z_ref1, y_ref1, x_ref1;
void align(glm::vec3 axis);
    float rotation, acc, speed;
    glm::mat4 rotation_matrix;
    float x_length, y_length, z_length;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    int tick();
    int bullet_tick();
    bounding_box_t bounding_box();
private:
    VAO *object;
};

#endif // Cuboid_H
