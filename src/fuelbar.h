#include "main.h"
#include "line.h"
#include "circle.h"
#include "rectangle.h"

#ifndef FUELBAR_H
#define FUELBAR_H

using namespace std;

class Fuelbar {
public:
    Fuelbar() {}
    Fuelbar(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    float thick;
    void draw(glm::mat4 VP, int input);
    void set_position(float x, float y);
    float speed;
    Rect a,b,c,d,e,f;
    Line left, top, bottom, right;
private:
};

#endif // FUELBAR_H
