#define GLM_ENABLE_EXPERIMENTAL
#include "plane.h"
#include "main.h"

using namespace glm;

Plane::Plane(float x, float y, float z, color_t color) {
    this->position = glm::vec4(x, y, z, 0);
    this->rotation = mat4(1.0f);
    this->pitch = 0; this->roll = 0; this->yaw = 0;
    this->x_ref = glm::vec3(1,0,0);
    this->y_ref = glm::vec3(0,0,1);
    this->z_ref = glm::vec3(0,1,0);
    this->z_ref1 = glm::vec3(0,0,1);
    acceleration = 0.005;
    speed = -0.1f;
    float length = 3.0f;
    float radius = 0.5f;

    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    /*GLfloat vertex_buffer_data[] = {
    0.0f, 1.0f, 0.0f,
    -0.2f, -1.0f, 0.2f,
    0.2f, -1.0f, 0.2f,
    -0.2f, -1.0f, -0.2f,
    0.2f, -1.0f, -0.2f,
    0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    -0.2f, -1.0f, 0.2f,
    -0.2f, -1.0f, -0.2f,
    0.0f, 1.0f, 0.0f,
    0.2f, -1.0f, 0.2f,
    0.2f, -1.0f, -0.2f,
    0.0f, 0.1f, 0.0f,
    0.0f, -0.1f, 0.0f,
    1.0f, 0.0f, 0.0f,
    0.0f, 0.1f, 0.0f,
    0.0f, -0.1f, 0.0f,
    -1.0f, 0.0f, 0.0f
    }; */
    GLfloat vertex_buffer_data[] = {
    0.0f, 0.0f, -1.0f,
    -0.2f, 0.2f, 1.0f,
    0.2f, 0.2f, 1.0f,
    -0.2f, -0.2f, 1.0f,
    0.2f, -0.2f, 1.0f,
    0.0f, 0.0f, -1.0f,
    0.0f, 0.0f, -1.0f,
    -0.2f, 0.2f, 1.0f,
    -0.2f, -0.2f, 1.0f,
    0.0f, 0.0f, -1.0f,
    0.2f, 0.2f, 1.0f,
    0.2f, -0.2f, 1.0f,
    0.0f, 0.0f, -0.1f,
    0.0f, 0.0f, 0.1f,
    1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, -0.1f,
    0.0f, 0.0f, 0.1f,
    -1.0f, 0.0f, 0.0f
    };
    this->object = create3DObject(GL_TRIANGLES, 18, vertex_buffer_data, color, GL_FILL);
}

void Plane::rotateall(float yaw1, float pitch1, float roll1){
    glm::mat4 yaw_mat    = glm::rotate((float) (yaw1 * M_PI / 180.0f), this->z_ref);
    glm::mat4 pitch_mat    = glm::rotate((float) (pitch1 * M_PI / 180.0f), this->x_ref);
    glm::mat4 roll_mat    = glm::rotate((float) (roll1 * M_PI / 180.0f), this->y_ref);
    rotation *= yaw_mat*pitch_mat*roll_mat;
    this->z_ref1 = rotation[2];
    this->y_ref1 = rotation[1];
}

void Plane::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (vec3(this->position.x, this->position.y, this->position.z));    // glTranslatef
    glm::mat4 myScalingMatrix;
    float scalingfactor = 1.0f;
    myScalingMatrix[0] = vec4(scalingfactor, 0.0, 0.0, 0.0);
    myScalingMatrix[1] = vec4(0.0, scalingfactor, 0.0, 0.0);
    myScalingMatrix[2] = vec4(0.0, 0.0, scalingfactor, 0.0);
    myScalingMatrix[3] = vec4(0.0, 0.0, 0.0, 1.0f);

    Matrices.model *= (translate * rotation * myScalingMatrix);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Plane::set_position(float x, float y, float z) {
    this->position = glm::vec4(x, y, z, 0);
}

void Plane::boost(int flag){
    if (flag>0)
        speed -= acceleration;
    else 
        speed += acceleration;
    //position.x  += z_ref1[0]*speed;
    //position.y  += z_ref1[1]*speed;
    //position.z  += z_ref1[2]*speed;

}

void Plane::tick() {
    position.x  += z_ref1[0]*speed;
    position.y  += z_ref1[1]*speed;
    position.z  += z_ref1[2]*speed;
    //printf("x:%f, y:%f, z:%f\n", position.x, position.y, position.z);
    //position.x  += z_ref1[0]*speed;
    //position.y  += z_ref1[1]*speed;
    //position.z  += z_ref1[2]*speed;
    //this->rotation += speed;
    // this->position.x -= speed;
    // this->position.y -= speed;
}

bounding_box_t Plane::bounding_box() {
    float x = this->position.x, y = this->position.y;
    float z = this->position.z;
    bounding_box_t box;
    box.type = -1;
    box.x = position.x-0.2; box.y = position.y-0.2; box.z = position.z-1;
    box.x_length = 0.4; box.y_length = 0.4; box.z_length = 2.0;
    return box;
}