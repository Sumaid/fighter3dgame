#include <cmath>
#include <vector>
#include <utility>
#include "rectangle.h"

using namespace std; 

Rect::Rect(float x, float y, float z, float length, float breadth, color_t color) {
    this->position = glm::vec3(x, y, z);
    this->rotation = 0;
    speed = 0.01;
    this->length = length;
    this->breadth = breadth;
    
    GLfloat vertex_buffer_data[] = {
        0.0f, 0.0f, 0.0f, // triangle 1 : begin
        0.0f, breadth, 0.0f,
         length, 0.0f, 0.0f, // triangle 1 : end
        length, 0.0f, 0.0f, // triangle 2 : begin
        0.0f, breadth, 0.0f,
        length, breadth,0.0f, // triangle 2 : end
    };
    this->object = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, COLOR_GREEN, GL_FILL);
}

void Rect::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Rect::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

bounding_box_t Rect::bounding_box() {
    float x = this->position.x, y = this->position.y;
    bounding_box_t box;
    box.type = 0;
    box.x = x; box.y = y;
    box.width = length; box.height = length;
    return box;
}
