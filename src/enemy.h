#include "main.h"
#include "line.h"
#include "cuboid.h"
#include "cone.h"
#include "rectangle.h"

#ifndef ENEMY_H
#define ENEMY_H

using namespace std;

class Enemy {
public:
    Enemy() {}
    Enemy(float x, float y, float z);
    glm::vec3 position;
    float rotation;
    float thick;
    void draw(glm::mat4 VP);
    void set_position(float x, float y, float z);
    int tick();
    float speed;
    float baseradius;
    float mountain_radius;
    float mountain_length;
    Cuboid a,b,c,d;
    Cone mountain;
    Cuboid base;
    bounding_box_t bounding_box();
private:
};

#endif // Enemy_H
