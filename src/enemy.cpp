#include <vector>
#include "line.h"
#include "cuboid.h"
#include "cone.h"
#include "enemy.h"
#define GLM_ENABLE_EXPERIMENTAL

using namespace std; 

Enemy::Enemy(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
    this->speed = 0.01;
    thick = 0.03f;
    baseradius = 5.0;
    mountain_radius = 1.75/2;
    mountain_length = 4.75/6;
    a = Cuboid(x+mountain_radius/4, y-mountain_radius/2, z+mountain_radius/4, 0.01, mountain_radius, 0.01, COLOR_DARKSLATEGREY);
    b = Cuboid(x-mountain_radius/4, y-mountain_radius/2, z+mountain_radius/4, 0.01, mountain_radius, 0.01, COLOR_DARKSLATEGREY);
    c = Cuboid(x+mountain_radius/4, y-mountain_radius/2, z-mountain_radius/4, 0.01, mountain_radius, 0.01, COLOR_DARKSLATEGREY);
    d = Cuboid(x-mountain_radius/4, y-mountain_radius/2, z-mountain_radius/4, 0.01, mountain_radius, 0.01, COLOR_DARKSLATEGREY);
    base = Cuboid(x, y-mountain_radius, z, mountain_radius/2, mountain_radius/2, mountain_radius/2, COLOR_RED);
    mountain = Cone(x, y, z, mountain_length, mountain_radius/2, COLOR_CREAM);
    a.acc = 0; b.acc = 0; c.acc = 0; d.acc = 0;
    base.acc = 0; mountain.acc=0;
    a.speed = speed; b.speed = speed; c.speed = speed; d.speed = speed;
    base.speed = speed; mountain.speed=speed;
}

void Enemy::draw(glm::mat4 VP) {
    a.draw(VP);
    b.draw(VP);
    c.draw(VP);
    d.draw(VP);
    base.draw(VP);
    mountain.draw(VP);
}

void Enemy::set_position(float x, float y, float z) {
    this->position = glm::vec3(x, y, z);
}

int Enemy::tick() {
    a.tick();
    b.tick();
    c.tick();
    d.tick();
    mountain.tick();
    if (base.tick())
        return 1;
    return 0;
}

bounding_box_t Enemy::bounding_box() {
    float x = mountain.position.x, y = mountain.position.y;
    float z = mountain.position.z;
    bounding_box_t box;
    box.type = 0;
    box.x = x-x-mountain_radius/4; box.y = y+mountain_length; box.z = z-mountain_radius/4;
    box.x_length = mountain_radius/2; 
    box.y_length = mountain_length+1.5*mountain_radius;
    box.z_length = mountain_radius/2;
    return box;
}